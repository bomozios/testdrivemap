package com.bzios.testdrivemap.model;

import com.orm.SugarRecord;

/**
 * Created by bzios-comsate on 3/5/2558.
 */
public class MapPosition extends SugarRecord<MapPosition> {

    String lat;
    String lng;

    public MapPosition() {
    }

    public MapPosition(String lat, String lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
