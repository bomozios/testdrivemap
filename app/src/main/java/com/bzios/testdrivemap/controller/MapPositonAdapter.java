package com.bzios.testdrivemap.controller;

import com.bzios.testdrivemap.model.MapPosition;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bzios-comsate on 4/5/2558.
 */
public class MapPositonAdapter extends BaseAdapter {

    private Context context;
    private List<MapPosition> mapPositions = new ArrayList<>();
    private LayoutInflater inflater;

    public MapPositonAdapter(Context context, List<MapPosition> mapPositions) {
        this.context = context;
        this.mapPositions = mapPositions;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mapPositions.size();
    }

    @Override
    public MapPosition getItem(int position) {
        return mapPositions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        MapPosition mapPosition = getItem(position);
        textView.setText(mapPosition.getLat() + ", " + mapPosition.getLng());
        return convertView;
    }
}
