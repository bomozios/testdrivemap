package com.bzios.testdrivemap.fragment;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.bzios.testdrivemap.R;
import com.bzios.testdrivemap.dao.MapPositionDao;
import com.bzios.testdrivemap.model.MapPosition;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bzios-comsate on 4/5/2558.
 */
public class OnMapFragment extends Fragment implements OnMapReadyCallback {

    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private MapPositionDao mMapPositionDao;
    private List<MapPosition> mMapPositionList;

    public static OnMapFragment newInstance() {
        OnMapFragment onMapFragment = new OnMapFragment();
        return onMapFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        View view = inflater.inflate(R.layout.fragment_onmap, container, false);

        setUpMapIfNeeded();
        return view;
    }

    public void setUpMapIfNeeded() {
        FragmentManager fragmentManager = getChildFragmentManager();
        mMapFragment = (SupportMapFragment) fragmentManager.findFragmentById(R.id.maps);
        mMapFragment.getMapAsync(this);
    }

    public void downloadData() {
        mMapPositionDao = new MapPositionDao(getActivity());
        mMapPositionList = new ArrayList<>();
        mMapPositionList = mMapPositionDao.getAll();
        if (mMapPositionList == null) {
            return;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        googleMap.setMyLocationEnabled(true);
        downloadData();
        BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.defaultMarker();
        for (MapPosition mapPosition : mMapPositionList) {
            double lat = Double.parseDouble(mapPosition.getLat());
            double lng = Double.parseDouble(mapPosition.getLng());
            LatLng latLng = new LatLng(lat, lng);
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng)
                    .title(String.valueOf(mapPosition.getId()))
                    .icon(bitmapDescriptor);
            googleMap.addMarker(markerOptions);
        }

    }
}
