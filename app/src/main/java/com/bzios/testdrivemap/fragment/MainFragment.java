package com.bzios.testdrivemap.fragment;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bzios.testdrivemap.R;
import com.bzios.testdrivemap.controller.MapPositonAdapter;
import com.bzios.testdrivemap.dao.MapPositionDao;
import com.bzios.testdrivemap.helper.LatLngValidation;
import com.bzios.testdrivemap.model.MapPosition;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainFragment extends Fragment implements View.OnClickListener {

    @InjectView(R.id.text_latitude) AutoCompleteTextView textLatitude;
    @InjectView(R.id.text_longitude) AutoCompleteTextView textLongitude;
    @InjectView(R.id.button_add_position) Button butAddPos;
    @InjectView(R.id.button_show_on_map) Button butShowMap;
    @InjectView(R.id.button_show_on_list) Button butShowList;

    private MapPositionDao mMapPositionDao;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMapPositionDao = new MapPositionDao(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, view);
        butAddPos.setOnClickListener(this);
        butShowMap.setOnClickListener(this);
        butShowList.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_add_position:
                addMapPosition();
                break;
            case R.id.button_show_on_map:
                OnMapFragment mapFragment = OnMapFragment.newInstance();
                getFragmentManager().beginTransaction().replace(android.R.id.content, mapFragment)
                        .addToBackStack(null).commit();
                break;
            case R.id.button_show_on_list:
                showList();
                break;
            case R.id.but_clear_db:
                mMapPositionDao.removeAll();
                break;
            default:
                break;
        }
    }

    public void addMapPosition() {
        String lat = textLatitude.getText().toString();
        if (!LatLngValidation.isMatched(lat, LatLngValidation.LATITUDE)) {
            notifyUser("Error..!");
            clearText();
            return;
        }
        String lng = textLongitude.getText().toString();
        if (!LatLngValidation.isMatched(lng, LatLngValidation.LONGITUDE)) {
            notifyUser("Error..!");
            clearText();
            return;
        }
        MapPosition mapPosition = mMapPositionDao.addData(lat, lng);
        if (mapPosition == null) {
            notifyUser("Error..!");
            clearText();
            return;
        } else {
            notifyUser("Added..!");
            clearText();
        }
    }

    public void showList() {
        new MaterialDialog.Builder(getActivity())
                .title("All Position")
                .adapter(new MapPositonAdapter(getActivity(), mMapPositionDao.getAll()),
                        new MaterialDialog.ListCallback() {
                            @Override
                            public void onSelection(MaterialDialog materialDialog, View view, int i,
                                    CharSequence charSequence) {
                                materialDialog.dismiss();
                            }
                        })
                .show();
    }

    public void clearText() {
        textLatitude.setText(null);
        textLongitude.setText(null);
    }

    public void notifyUser(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
