package com.bzios.testdrivemap.helper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by bzios-comsate on 3/5/2558.
 */
public class LatLngValidation {

    public static final int LATITUDE = 0;
    public static final int LONGITUDE = 1;

    private static final String LAT_PATTERN
            = "^[-+]?([1-8]?\\d(\\.\\d+)?|90(\\.0+)?)";
    private static final String LNG_PATTERN =
            "\\s*[-+]?(180(\\.0+)?|((1[0-7]\\d)|([1-9]?\\d))(\\.\\d+)?)$";

    public static boolean isMatched(String hex, int type) {
        String pattern = "";
        switch (type) {
            case 0:
                pattern = LAT_PATTERN;
                break;
            case 1:
                pattern = LNG_PATTERN;
        }
        Pattern mPattern = Pattern.compile(pattern);
        Matcher mMatcher = mPattern.matcher(hex);
        return mMatcher.matches();
    }
}
