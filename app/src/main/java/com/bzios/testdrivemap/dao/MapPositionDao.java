package com.bzios.testdrivemap.dao;

import com.bzios.testdrivemap.model.MapPosition;

import android.content.Context;

import java.util.List;

/**
 * Created by bzios-comsate on 3/5/2558.
 */
public class MapPositionDao {

    private Context context;

    public MapPositionDao(Context context) {
        this.context = context;
    }

    public MapPosition addData(String lat, String lng) {
        MapPosition mapPosition = new MapPosition(lat, lng);
        mapPosition.save();
        return mapPosition;
    }

    public List<MapPosition> getAll() {
        List<MapPosition> mapPositionList = MapPosition.listAll(MapPosition.class);
        return mapPositionList;
    }

    public void removeAll() {
        MapPosition.deleteAll(MapPosition.class);
    }

    public MapPosition getById(long id) {
        MapPosition mapPosition = MapPosition.findById(MapPosition.class, id);
        return mapPosition;
    }
}
